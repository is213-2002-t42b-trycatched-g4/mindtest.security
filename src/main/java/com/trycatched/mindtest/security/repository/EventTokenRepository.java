package com.trycatched.mindtest.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trycatched.mindtest.security.entity.EventToken;

@Repository
public interface EventTokenRepository extends JpaRepository<EventToken, String>{

}
