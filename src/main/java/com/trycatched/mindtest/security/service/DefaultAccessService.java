package com.trycatched.mindtest.security.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trycatched.mindtest.security.constant.ApiError;
import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.repository.EventTokenRepository;

@Service
public class DefaultAccessService implements AccessService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private EventTokenRepository eventTokenRepository;

	@Autowired
	public DefaultAccessService(EventTokenRepository eventTokenRepository) {
		this.eventTokenRepository = eventTokenRepository;
	}

	@Override
	public ApiResponse isTokenValid(String token) {

		boolean existsToken = eventTokenRepository.existsById(token);
		logger.info("Existe token? {}", existsToken);

		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), existsToken);
	}

}
