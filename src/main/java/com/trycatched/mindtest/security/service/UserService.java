package com.trycatched.mindtest.security.service;

import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.dto.LoginRequest;
import com.trycatched.mindtest.security.dto.SignupRequest;
import com.trycatched.mindtest.security.exception.ApiException;

public interface UserService {

	ApiResponse authenticate(LoginRequest loginRequest) throws ApiException;

	ApiResponse createAccount(SignupRequest signupRequest) throws ApiException;

	ApiResponse existsUser(Long userId);

}
