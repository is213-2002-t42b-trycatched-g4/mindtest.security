package com.trycatched.mindtest.security.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.trycatched.mindtest.security.constant.ApiError;
import com.trycatched.mindtest.security.constant.ERole;
import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.dto.JwtResponse;
import com.trycatched.mindtest.security.dto.LoginRequest;
import com.trycatched.mindtest.security.dto.SignupRequest;
import com.trycatched.mindtest.security.entity.EventToken;
import com.trycatched.mindtest.security.entity.Role;
import com.trycatched.mindtest.security.entity.User;
import com.trycatched.mindtest.security.exception.ApiException;
import com.trycatched.mindtest.security.repository.EventTokenRepository;
import com.trycatched.mindtest.security.repository.RoleRepository;
import com.trycatched.mindtest.security.repository.UserRepository;
import com.trycatched.mindtest.security.secure.JwtUtils;

@Service
public class DefaultUserService implements UserService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private UserRepository userRepository;

	private RoleRepository roleRepository;

	private PasswordEncoder encoder;

	private AuthenticationManager authenticationManager;

	private JwtUtils jwtUtils;

	private EventTokenRepository eventTokenRepository;

	@Autowired
	public DefaultUserService(UserRepository userRepository, RoleRepository roleRepository,
			PasswordEncoder encoder, AuthenticationManager authenticationManager, JwtUtils jwtUtils,
			EventTokenRepository eventTokenRepository) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.encoder = encoder;
		this.authenticationManager = authenticationManager;
		this.jwtUtils = jwtUtils;
		this.eventTokenRepository = eventTokenRepository;
	}

	@Override
	public ApiResponse createAccount(SignupRequest signUpRequest) throws ApiException {
		User responseUser;

		logger.debug("signUpRequest: {}", signUpRequest);
		boolean userExists = userRepository.existsByUsername(signUpRequest.getUsername());
		if (userExists) {
			throw ApiException.of(ApiError.USER_ALREADY.getCode(), ApiError.USER_ALREADY.getMessage());
		}

		boolean emailExists = userRepository.existsByEmail(signUpRequest.getEmail());
		if (emailExists) {
			throw ApiException.of(ApiError.EMAIL_USED.getCode(), ApiError.EMAIL_USED.getMessage());
		}

		logger.debug("Creando nueva cuenta");
		User user = new User(signUpRequest.getName(), signUpRequest.getAge(), signUpRequest.getTelephone(),
				signUpRequest.getSex(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		logger.debug("strRoles: {}", strRoles);
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException(ApiError.ROL_NOT_FOUND.getMessage()));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException(ApiError.ROL_NOT_FOUND.getMessage()));
					roles.add(adminRole);

					break;
				case "visit":
					Role modRole = roleRepository.findByName(ERole.ROLE_VISIT)
					.orElseThrow(() -> new RuntimeException(ApiError.ROL_NOT_FOUND.getMessage()));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException(ApiError.ROL_NOT_FOUND.getMessage()));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		responseUser = userRepository.save(user);

		return ApiResponse.of(ApiError.USER_REGISTERED.getCode(), ApiError.USER_REGISTERED.getMessage(), responseUser);
	}

	@Override
	public ApiResponse authenticate(LoginRequest loginRequest) throws ApiException {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		logger.debug("Token: {}", jwt);

		logger.debug("Saving token");
		eventTokenRepository.save(new EventToken(jwt, new Date()));
		logger.debug("Token saved");

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());

		JwtResponse response = new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles);
		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), response);
	}

	@Override
	public ApiResponse existsUser(Long userId) {
		boolean exists = userRepository.existsById(userId);
		logger.info("Existe usuario? {}", exists);
		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), exists);
	}

}
