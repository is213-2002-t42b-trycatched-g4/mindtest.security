package com.trycatched.mindtest.security.service;

import com.trycatched.mindtest.security.dto.ApiResponse;

public interface AccessService {

	public ApiResponse isTokenValid(String token);

}
