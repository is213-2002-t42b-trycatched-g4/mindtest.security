package com.trycatched.mindtest.security.constant;

public enum ApiError {

	SUCCESS("0", "Ok"),
	NO_APPLICATION_PROCESSED("9000", "El sistema no pudo procesar su solicitud"),
	EMPTY_OR_NULL_PARAMETER("9001", "Uno o más parámetros están vacíos o nulos"),
	USER_NOT_FOUND("9002", "Usuario no encontrado"),
	ROL_NOT_FOUND("9003", "Rol no encontrado"),
	USER_REGISTERED("8000", "Usuario registrado correctamente"),
	USER_ALREADY("8001", "Error: Usuario ya ha sido registrado"),
	EMAIL_USED("8002", "Error: Email ya está en uso");

	private final String code;
	private final String message;

	private ApiError(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
