package com.trycatched.mindtest.security.constant;

public enum ERole {

	ROLE_USER,
	ROLE_VISIT,
	ROLE_ADMIN

}
