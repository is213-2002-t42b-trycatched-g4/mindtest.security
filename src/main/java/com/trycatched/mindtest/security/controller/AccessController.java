package com.trycatched.mindtest.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.service.AccessService;

@RestController
@RequestMapping("/api/access")
public class AccessController {

	private AccessService accessService;

	@Autowired
	public AccessController(AccessService accessService) {
		this.accessService = accessService;
	}

	@GetMapping("/validate/{token}")
	public ResponseEntity<?> isTokenValid(@PathVariable("token") String token) {
		ApiResponse response = accessService.isTokenValid(token);
		return ResponseEntity.ok(response);
	}
}
