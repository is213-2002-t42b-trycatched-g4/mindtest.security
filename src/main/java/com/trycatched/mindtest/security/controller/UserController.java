package com.trycatched.mindtest.security.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.dto.ErrorResponse;
import com.trycatched.mindtest.security.dto.LoginRequest;
import com.trycatched.mindtest.security.dto.SignupRequest;
import com.trycatched.mindtest.security.exception.ApiException;
import com.trycatched.mindtest.security.service.UserService;

@RestController
@RequestMapping("/api/auth")
public class UserController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	//	@PostMapping("/users")
	//	public ResponseEntity<?> createUser(@RequestBody String request) {
	//		ApiResponse response;
	//		try {
	//			response = userService.createUser(request);
	//		} catch (ApiException e) {
	//			logger.error(e.getMessage(), e);
	//			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
	//		}
	//		return ResponseEntity.ok(response);
	//	}
	//
	//	@GetMapping("/users/{id}")
	//	public ResponseEntity<?> getUser(@PathVariable("id") Integer id) {
	//		ApiResponse response;
	//		try {
	//			response = userService.findById(id);
	//		} catch (ApiException e) {
	//			logger.error(e.getMessage(), e);
	//			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
	//		}
	//		return ResponseEntity.ok(response);
	//	}

	//	@Autowired
	//	AuthenticationManager authenticationManager;

	//	@Autowired
	//	UserRepository userRepository;
	//
	//	@Autowired
	//	RoleRepository roleRepository;
	//
	//	@Autowired
	//	PasswordEncoder encoder;
	//
	//	@Autowired
	//	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		ApiResponse response;
		try {
			response = userService.authenticate(loginRequest);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}

		return ResponseEntity.ok(response);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		ApiResponse response;
		try {
			response = userService.createAccount(signUpRequest);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}

		return ResponseEntity.ok(response);
	}

}
