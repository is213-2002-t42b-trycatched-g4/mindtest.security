package com.trycatched.mindtest.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.security.dto.ApiResponse;
import com.trycatched.mindtest.security.service.UserService;

@RestController
@RequestMapping("/api/validation")
public class UserValidationController {

	private UserService userService;

	@Autowired
	public UserValidationController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/exists/{userId}")
	public ResponseEntity<?> existsUser(@PathVariable Long userId) {
		ApiResponse response = userService.existsUser(userId);
		return ResponseEntity.ok(response);
	}

}
