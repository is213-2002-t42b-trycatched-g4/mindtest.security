package com.trycatched.mindtest.security.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "event_token")
public class EventToken implements Serializable {

	private static final long serialVersionUID = -6161318972263369229L;

	@Id
	@Column(name = "id")
	private String id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "time_register")
	private Date timeRegister;

	public EventToken() {

	}

	public EventToken(String id, Date timeRegister) {
		super();
		this.id = id;
		this.timeRegister = timeRegister;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTimeRegister() {
		return timeRegister;
	}

	public void setTimeRegister(Date timeRegister) {
		this.timeRegister = timeRegister;
	}

	@Override
	public String toString() {
		return "EventToken [id=" + id + ", timeRegister=" + timeRegister + "]";
	}

}
