package com.trycatched.mindtest.security.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SignupRequest {

	private String name;
	private String age;
	private String telephone;
	private String sex;
	private String username;
	private String password;
	private String email;
	private Set<String> role;

	@JsonCreator
	public SignupRequest(@JsonProperty("name") String name, @JsonProperty("age") String age,
			@JsonProperty("telephone") String telephone, @JsonProperty("sex") String sex,
			@JsonProperty("username") String username, @JsonProperty("email") String email,
			@JsonProperty("password") String password, @JsonProperty("roles") Set<String> role) {
		this.name = name;
		this.age = age;
		this.telephone = telephone;
		this.sex = sex;
		this.username = username;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public String getAge() {
		return age;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getSex() {
		return sex;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public Set<String> getRole() {
		return role;
	}

	@Override
	public String toString() {
		return "SignupRequest [name=" + name + ", age=" + age + ", telephone=" + telephone + ", sex=" + sex
				+ ", username=" + username + ", password=" + password + ", email=" + email + ", role=" + role + "]";
	}

}
